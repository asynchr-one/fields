// @ts-ignore
import React, {Component} from "react";
import styled from "styled-components";
import RadioButton from "../RadioButton";

class RadioChooser extends Component<any, any> {
    constructor(props) {
        super(props);
        this.state = {
            title: props.title,
            name: props.name,
            zone: props.zone,
            bg: props.bg ?? "https://images.unsplash.com/photo-1653314622658-4a34c996b410?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw1NHx8fGVufDB8fHx8&auto=format&fit=crop&w=500&q=60",
            slug: props.slug,
            label: props.label,
            disabled: props.name + "-red",
            enabled: props.name + "-green",
            selectedOption: "",
        };

        this.onValueChange = this.onValueChange.bind(this);
        this.formSubmit = this.formSubmit.bind(this);
    }

    onValueChange(event) {
        this.setState({
            selectedOption: event.target.value
        });
    }

    formSubmit(event) {
        event.preventDefault();
    }

    render() {
        const RadioImg = styled.img`
          border-radius: 50%;
          border: 0.5vh solid white;
          background-color: white;
          height: 10vh;
          width: 10vh;
          top: 5vh;
          position: relative;
          left: 0vw;
        `;
        const Icon = styled.img`
          border: 0.5vh solid white;
          border-radius: 50%;
          background-color: white;
        `;
        const RadioWrapper = styled.div`
          width: 100%;
          height: 50vh;
        `;

        const RadioButtons = styled.div`
          text-align: center;
          margin: 0 auto;
          background-image: url(${this.state.bg});
          background-repeat: no-repeat;
          background-position: center;
          background-size: cover;
          height: 40vh;
        `;

        const WhiteBar = styled.div`
          height: 10vh;
          background-color: white;
          width: 100%;
          position: absolute;
          top: 10vh;
        `

        const Zone = styled.h3`
          width: 100%;
          display: block;
          padding: 0px 2vw;
        `

        const Legend = styled.div`
          width: 100%;
          display: flex;
          position: absolute;
        `
        const RadioTitle = styled.h4`
          text-align: right;
          height: 6vh;
          width: 45vw;
          padding: 0px 2vw;
          margin: 11vh 0px;
          font-size: 1em;
        `
        const RadioSubtitle = styled.h5`
          text-align: left;
          font-weight: normal;
          height: 6vh;
          width: 45vw;
          padding: 0px 2vw;
          font-size: 1em;
          margin: 11vh 0px;
        `

        const imgEnabled = "/img/noun/100px/" + this.state.enabled + "-100.png";
        const imgDisabled = "/img/noun/100px/" + this.state.disabled + "-100.png";

        return <RadioWrapper>
            <RadioButtons>
                <RadioButton type="enabled" value={this.state.enabled} name={this.state.name} slug={this.state.slug} selectedOption={this.state.selectedOption} onValueChange={this.onValueChange} />
                <RadioButton type="disabled" value={this.state.disabled} name={this.state.name} slug={this.state.slug} selectedOption={this.state.selectedOption} onValueChange={this.onValueChange} />
                <WhiteBar />
                <Legend>
                    <RadioTitle>{this.state.title}</RadioTitle>
                    <RadioImg src={imgEnabled} />
                    <RadioSubtitle>{this.state.label}</RadioSubtitle>
                </Legend>
            </RadioButtons>
            <Zone>{this.state.zone}</Zone>
        </RadioWrapper>
    }
}

export default RadioChooser
