// @ts-ignore
import React, {Component} from "react";
import styled from "styled-components";
import { AbstractDocument } from "./index"

export class Deliverable extends AbstractDocument {

    constructor(props) {
        super(props);
    }

    getColor() {
        return "red";
    }
}
