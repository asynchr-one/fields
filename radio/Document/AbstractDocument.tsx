// @ts-ignore
import React, {Component} from "react";
import styled from "styled-components";
import DocumentButton from "../DocumentButton";
import LanterneClient from "../../../../../src/meilisearch/LanterneClient";

export abstract class AbstractDocument extends Component<any, any> {
    constructor(props) {
        super(props);
        this.state = {
            blocKey: props.blocKey,
            title: props.title,
            name: props.name,
            zone: props.zone,
            bg: props.bg ?? "https://images.unsplash.com/photo-1653314622658-4a34c996b410?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw1NHx8fGVufDB8fHx8&auto=format&fit=crop&w=500&q=60",
            slug: props.slug,
            typeLabel: props.typeLabel,
            label: props.label,
            report: props.report,
            selectedOption: props.selectedOption,
            buttons: props.buttons
        };

        this.onValueChange = this.onValueChange.bind(this);
        this.formSubmit = this.formSubmit.bind(this);

        this.getButtons = this.getButtons.bind(this);
        this.getRadioButtons = this.getRadioButtons.bind(this);

    }

    getColor() {
        return "grey";
    }

    onValueChange(event) {
        this.setState({
            selectedOption: event.target.value
        });

        const report = this.props.report;
        let answers = report.answers;
        answers[this.state.blocKey] = {
            value: event.target.value,
            key: this.state.key,
            title: this.state.title,
            name: this.state.name,
            zone: this.state.zone,
            slug: this.state.slug,
            label: this.state.label
        };
        const record = {
            ...this.props.report,
            "answers": answers
        }
        this.props.updateReport(record);
    }

    formSubmit(event) {
        event.preventDefault();
    }

    getButtons() {
        const buttonTypes = require('../../../../../src/data/la-lanterne/buttons-array.json');
        let buttonType;
        const allButtons = this.state.buttons.map(button => (
            buttonType = buttonTypes[button.type],
            <DocumentButton
                type={buttonType.type}
                emoji={buttonType.emoji}
                label1={buttonType.label1}
                label2={buttonType.label2}
                name={this.state.name} slug={this.state.slug} selectedOption={this.state.selectedOption}
                onValueChange={this.onValueChange}/>
        ));
        return <>{allButtons}</>
    }

    getRadioButtons(buttons) {
        const RadioImg = styled.img`
          border-radius: 50%;
          margin-top: 1vh;
          margin-left: 2vw;
          height: 10vh;
          width: 10vh;
          background-color: white;
          border: 0.5vh solid white;
        `;
        const Icon = styled.img`
          border: 0.5vh solid white;
          border-radius: 50%;
          background-color: white;
        `;

        const RadioButtons = styled.div`
          text-align: center;
          margin: 0vh auto;
          position: relative;
          top: 10vh;
        `;

        const WhiteBar = styled.div`
          position: absolute;
          top: 3.5vh;
          height: 6vh;
          width: 100%;
          background-color: white;
          z-index: -2;
        `

        const Zone = styled.h3`
          width: 100%;
          display: block;
          padding: 0px 4vw;
          //top: -21vh;
          //position: relative;
        `

        const Legend = styled.div`
          display: flex;
          top: 0vh;
          position: fixed;
          width: 100%;
        `
        const RadioTitle = styled.h4`
          font-size: 1em;
          text-align: left;
          width: fit-content;
          margin-left: 2vh;
          margin-top: 4.5vh;
        `
        const RadioSubtitle = styled.h5`
          text-align: left;
          font-weight: normal;
          height: 6vh;
          width: 45vw;
          padding: 0px 2vw;
          font-size: 1em;
          margin: 11vh 0px;
        `

        const DoubleCrosses = styled.img`
          width: 32vw;
          top: 9vh;
          position: absolute;
          left: 35vw;
          z-index: 0;
        `

        const Buttons = styled.div`
          position: relative;
          top: 0;
          height: 100%;
          background-color: rgba(255, 255, 255, 0.8);
          width: 100%;
        `;

        const Type = styled.h5`
          position: absolute;
          left: 4vw;
          top: 10vh;
          font-size: 1.1em;
        `;

        const enabled = this.state.name + "-green"
        const disabled = this.state.name + "-red"
        const imgEnabled = "/img/noun/100px/" + enabled + "-100.png";
        const imgDisabled = "/img/noun/100px/" + disabled + "-100.png";
        const color = this.getColor();
        const doublecrosses = `/svg/cross-arrows-big-${color}.svg`;

        return (<>
            <Buttons>
                <RadioButtons>
                    <DoubleCrosses src={doublecrosses} />
                    {buttons}
                    <Legend>
                        <RadioImg src={imgEnabled}/>
                        <RadioTitle>{this.state.title}</RadioTitle>
                        <WhiteBar/>
                        <Type>{this.state.typeLabel}</Type>
                    </Legend>
                </RadioButtons>
            </Buttons>
            <Zone>{this.state.zone}</Zone>
        </>)
    }

    render() {
        const RadioWrapper = styled.div`
          width: 100%;
          height: 40vh;
          background-image: url(${this.state.bg});
          background-repeat: no-repeat;
          background-size: cover;
          background-position: center center;
          height: 45vh;
          position: relative;
          top: 0;
        `;

        const buttons = this.getButtons();
        const radioButtons = this.getRadioButtons(buttons);

        return <RadioWrapper>
            {radioButtons}
        </RadioWrapper>
    }
}
