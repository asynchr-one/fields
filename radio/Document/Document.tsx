// @ts-ignore
import React, {Component} from "react";
import styled from "styled-components";

import { AbstractDocument } from "./index"

class Document extends AbstractDocument {
    constructor(props) {
        super(props);
    }

    getColor() {
        return "blue";
    }

}

export default Document
