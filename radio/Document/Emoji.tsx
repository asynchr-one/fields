// @ts-ignore
import React, {Component} from "react";
import styled from "styled-components";
import DocumentButton from "../DocumentButton";

export class Emoji extends Component<any, any> {
    constructor(props) {
        super(props);
        this.state = {
            label1: props.label1,
            label2: props.label2,
            symbol: props.symbol
        };
    }

    render() {
        const EmojiItem = styled.div``;
        const Img = styled.div`
          font-size: 4vh;
        `;
        const TextHorz = styled.div`
          font-size: .8em;
          width: 20vw;
        `;
        const TextVert = styled.div`
          font-size: .8em;
          width: 20vw;
        `;

        const aria = `${this.state.label1} - ${this.state.label2}`;

        return (
            <EmojiItem aria-label={aria}>
                <Img role="img" className="image"
                     alt={this.state.symbol}>{String.fromCodePoint(this.state.symbol)}</Img>
                <TextHorz className="text horizontal">{this.state.label1}</TextHorz>
                <TextVert className="text vertical">{this.state.label2}</TextVert>
            </EmojiItem>
        );
    }

}
