// @ts-ignore
import React, {Component} from "react";
import styled from "styled-components";

class RadioButton extends Component<any, any> {
    constructor(props) {
        super(props);
        this.state = {
            type: props.type,
            value: props.value,
            name: props.name,
            slug: props.slug,
            selectedOption: props.selectedOption
        };
    }

    render() {
        const RadioButton = styled.div`
          float: left;
          width: 50%;
        `;
        const RadioInput = styled.input`
          display: none;
          position: relative;
          margin: 1vh auto;

          &:checked {
            + .bg-rectangle {
              &.disabled {
                background-color: rgba(240, 128, 128, .8);
              }
              &.enabled {
                background-color: rgba(144, 238, 144, .8);
              }
            }
          }
          
          + .bg-rectangle {
            &.disabled {
              background-color: rgba(240, 128, 128,.2);
            }
            &.enabled {
              background-color: rgba(144, 238, 144, .2);
            }
          }
        `;

        const BgRectangle = styled.div`
          padding: 20vh 0; 
        `;

        const RadioLabel = styled.label`
          display: inline-block;
          width: 100%;
        `;

        const RectangleClass = "bg-rectangle " + this.state.type;

        const name = this.state.slug + "_" + this.state.name;
        const id = this.state.slug + "_" + this.state.value;

        return  <RadioButton className="radio">
                    <RadioLabel className={this.state.type}>
                        <RadioInput
                            type="radio"
                            value={this.state.value}
                            id={id}
                            name={name}
                            checked={this.props.selectedOption === this.state.value}
                            onChange={this.props.onValueChange}
                        />
                        <BgRectangle className={RectangleClass} />
                    </RadioLabel>
                </RadioButton>
    }
}

export default RadioButton
