// @ts-ignore
import React, {Component} from "react";
import styled from "styled-components";
import { Emoji } from "../Document"

class DocumentButton extends Component<any, any> {
    constructor(props) {
        super(props);
        this.state = {
            type: props.type,
            name: props.name,
            label1: props.label1,
            label2: props.label2,
            slug: props.slug,
            emoji: props.emoji,
            selectedOption: props.selectedOption
        };
    }

    render() {
        const RadioButton = styled.div`
          width: 20vw;
          margin: 0 auto;
          position: absolute;
          z-index: 5;

          &.validated,
          &.useful {
            right: 2vw;
            top: 0px;
          }
          &.completed_updated {
            top: 8vh;
            right: 28vw;
          }
          &.completed_notupdated {
            top: 8vh;
            left: 30vw;
          }
          &.notcompleted_updated {
            top: 18vh;
            right: 28vw;
          }
          &.notcompleted_notupdated {
            top: 18vh;
            left: 30vw;
          }
          &.notexplained,
          &.donotexist {
            left: 2vw;
            top: 25vh;
          }
          &.delete {
            right: 2vw;
            top: 28vh;
          }
        `;
        const RadioInput = styled.input`
          display: none;
          position: relative;
          margin: 1vh auto;

          &:checked {
            + .bg-rectangle {
              .image {
                border: 5px solid lightcoral;
                border-radius: 25%;
                background-color: white;
                margin: 0px auto;
              }
              
              &.validated,
              &.useful {}
              &.completed_updated {}
              &.completed_notupdated {}
              &.notcompleted_updated {}
              &.notcompleted_notupdated {}
              &.notexplained,
              &.donotexist {}
              &.delete {}
              }
            }
          }

          + .bg-rectangle {
            .text {
              position: relative;
            }

            &.validated,
            &.useful {
              .horizontal {                 
                //top: -4.5vh;
                //left: -18vw;                 
                //text-align: right;               
              }
            }
            &.completed_updated { 
              .horizontal {display: none;}
              .vertical {
                top: 1.75vh;
                right: -12vw;
              }
            }
            &.completed_notupdated { 
              .horizontal {
                left: 12vw;
                top: -8vh;
              }
              .vertical {
                top: 0vh;
                left: -12vw;
              }
            }
            &.notcompleted_updated {
              .horizontal {display: none;}
              .vertical {display: none;}
            }
            &.notcompleted_notupdated {
              .horizontal {
                left: 12vw;
                top: 0vh;
              }
              .vertical {display: none;}
            }
            &.notexplained,
            &.donotexist {
              .horizontal {
                //top: -3.5vh;
                //right: -18vw;
                //text-align: left;
              }
            }
            &.delete {
              .horizontal {
                //top: -3.5vh;
                //right: -18vw;
                //text-align: left;
              }
              
              .image {
                font-size: 3vh;
                height: auto;
                width: auto;
                margin: 0px;
                padding: 0px;
                border: none;
                background-color: transparent;
              }
            }

            .image{
              height: 5vh;
              margin: 5px auto;
              width: 5vh;
              padding-top: 0.4vh;
            }
          }
        `;

        const BgRectangle = styled.div`
          width: 20vw;
        `;

        const RadioLabel = styled.label``;

        const buttonClass = (this.state.type) ? this.state.type : "delete";
        const RectangleClass = "bg-rectangle " + buttonClass;

        const name = this.state.slug + "_" + this.state.name;
        const id = name + "_" + this.state.type;

        return  <RadioButton className={buttonClass}>
                    <RadioLabel>
                        <RadioInput
                            type="radio"
                            value={this.state.type}
                            id={id}
                            name={name}
                            checked={this.props.selectedOption === this.state.type}
                            onChange={this.props.onValueChange}
                        />
                        <BgRectangle className={RectangleClass}>
                            <Emoji key={id} symbol={this.state.emoji} label1={this.state.label1} label2={this.state.label2} />
                        </BgRectangle>
                    </RadioLabel>
                </RadioButton>
    }
}

export default DocumentButton
